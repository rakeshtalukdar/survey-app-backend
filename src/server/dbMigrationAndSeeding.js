const { connection, executeQueryPromise } = require('./dbConnection');

const CREATE_SURVEYS_TABLE = `CREATE TABLE IF NOT EXISTS Surveys (
        uuid VARCHAR(36) PRIMARY KEY,
        title TEXT NOT NULL,
        survey_category_uuid VARCHAR(36) NOT NULL,
        open_date DATE,
        close_date DATE,
        status ENUM('created', 'active', 'closed') NOT NULL,
        created_at DATETIME,
        updated_at DATETIME
    );`;



const CREATE_QUESTIONS_TABLE = `CREATE TABLE IF NOT EXISTS Questions (
        uuid VARCHAR(36) PRIMARY KEY,
        question TEXT NOT NULL,
        question_type_uuid VARCHAR(36) NOT NULL REFERENCES Question_Types(uuid),
        survey_uuid VARCHAR(36) NOT NULL REFERENCES Surveys(uuid),
        created_at DATETIME,
        updated_at DATETIME
    );`;



const CREATE_QUESTION_TYPES_TABLE = `CREATE TABLE IF NOT EXISTS Question_Types (
        uuid VARCHAR(36) PRIMARY KEY,
        type VARCHAR(255) NOT NULL UNIQUE,
        created_at DATETIME,
        updated_at DATETIME
    );`;



const CREATE_ANSWERS_TABLE = `CREATE TABLE IF NOT EXISTS Answers (
        uuid VARCHAR(36) PRIMARY KEY,
        answer TEXT NOT NULL,
        question_uuid VARCHAR(36) NOT NULL REFERENCES Questions(uuid),
        created_at DATETIME ,
        updated_at DATETIME
    );`;



const CREATE_USERS_TABLE = `CREATE TABLE IF NOT EXISTS Users (
        uuid VARCHAR(36) PRIMARY KEY,
        email TEXT NOT NULL,
        created_at DATETIME ,
        updated_at DATETIME 
    );`;



const CREATE_SURVEYS_AND_RESPONSES_TABLE = `CREATE TABLE IF NOT EXISTS Surveys_And_Responses (
        uuid VARCHAR(36) PRIMARY KEY,
        survey_uuid VARCHAR(36) NOT NULL REFERENCES Surveys(uuid),
        question_uuid VARCHAR(36) NOT NULL REFERENCES Questions(uuid),
        answer TEXT,
        user_uuid VARCHAR(36) NOT NULL REFERENCES Users(uuid),
        created_at DATETIME ,
        updated_at DATETIME 
    );`;



const CREATE_SURVEY_CATEGORIES_TABLE = `CREATE TABLE IF NOT EXISTS Survey_Categories (
        uuid VARCHAR(36) PRIMARY KEY,
        category VARCHAR(255) NOT NULL UNIQUE,
        created_at DATETIME  ,
        updated_at DATETIME 
    );`;


const CREATE_BOOLEAN_ANSWERS_TABLE = `CREATE TABLE IF NOT EXISTS Boolean_Answers (
        uuid VARCHAR(36) PRIMARY KEY,
        answer VARCHAR(255) NOT NULL UNIQUE,
        created_at DATETIME ,
        updated_at DATETIME 
    );`;




// eslint-disable-next-line no-unused-vars
const databaseMigration = (async () => {
    try {

        const promiseArray = [
            executeQueryPromise(CREATE_SURVEYS_TABLE),
            executeQueryPromise(CREATE_SURVEY_CATEGORIES_TABLE),
            executeQueryPromise(CREATE_USERS_TABLE),
            executeQueryPromise(CREATE_QUESTIONS_TABLE),
            executeQueryPromise(CREATE_QUESTION_TYPES_TABLE),
            executeQueryPromise(CREATE_ANSWERS_TABLE),
            executeQueryPromise(CREATE_BOOLEAN_ANSWERS_TABLE),
            executeQueryPromise(CREATE_SURVEYS_AND_RESPONSES_TABLE),
        ];

        await Promise.all(promiseArray);
        databaseSeeding();
    } catch (error) {
        let errorMessage = 'Oops!! Error occurred.'
        handleDbError(errorMessage, error);
    }
});


const USERS_TABLE_SEEDING = `INSERT INTO Users(uuid, email)
VALUES('5eb8cc3a-a222-461a-a011-2dbbbdb42655', 'a@e.com'),
      ('7310f85b-922d-4e34-b415-53a3c3ccb33b', 'b.@e.com'),
      ('3006a28e-ad7c-4b35-974a-35f1fb6256ab', 'c@e.com'),
      ('b8784da4-e8f1-4329-bd4a-bddcefb2c8e3','e@e.com'),
      ('20c6e415-a2d8-4b1e-b90c-29ed01f14136','f@com'),
      ('0ca04ce5-ade8-4d2a-8e8a-285be0578e18', 'd@e.com' );`;



const SURVEY_CATEGORIES_TABLE_SEEDING = `INSERT INTO Survey_Categories(uuid, category)
VALUES('62d47445-aab1-4233-b03c-3ec9effdc251', 'Information Technology'),
      ('c2a840a0-9bbc-43da-ac9b-617f94ca5bc9', 'Healthcare'),
      ('b9ebe3eb-7deb-4070-9861-7c0633dafb52', 'Political'),
      ('b7dc8ecb-1b23-4a1e-a918-ccecd13701d8', 'Agriculture' );`;


const SURVEYS_TABLE_SEEDING = `INSERT INTO Surveys(uuid, title, survey_category_uuid, open_date, close_date, status)
VALUES('14fef932-5cdd-426e-b877-80dfc45d6869', 'Mobile App Feedback', '62d47445-aab1-4233-b03c-3ec9effdc251', '2020-09-18', '2020-09-22', 1),
      ('905a4f11-cc27-4fad-9337-0856788edda8', 'Healthcare Feedback', 'c2a840a0-9bbc-43da-ac9b-617f94ca5bc9', '2020-09-19', '2020-09-23', 1),
      ('5844fa14-e632-47dc-beb7-7ce265144bc2', 'Political Agenda', 'b9ebe3eb-7deb-4070-9861-7c0633dafb52', '2020-09-20', '2020-09-22', 1),
      ('77774f11-cc27-4fad-9337-0856788edda8', 'Healthcare Feedback', 'c2a840a0-9bbc-43da-ac9b-617f94ca5bc9', '2020-09-18', '2020-09-30', 3),
      ('a93f4f11-cc27-4fad-9337-0856788edda8', 'Agriculture', 'c2a840a0-9bbc-43da-ac9b-617f94ca5bc9', '2020-09-18', '2020-09-28', 2),
      ('6666fa14-e632-47dc-beb7-7ce265144bc2', 'Medias role in politics', 'b9ebe3eb-7deb-4070-9861-7c0633dafb52', '2020-10-19', '2020-11-23', 3),
      ('60bf6a57-ff68-41ff-bb18-8cb1cbc5a181', 'Food Quality In India', 'b7dc8ecb-1b23-4a1e-a918-ccecd13701d8', '2020-09-18', '2020-09-22', 2);`;



const QUESTION_TYPES_TABLE_SEEDING = `INSERT INTO Question_Types(uuid, type)
VALUES('51552849-e186-4160-90eb-f32ea6437d14', 'MCQ'),
      ('9ca3c631-98bf-4c18-a5a7-d85f39a7869e', 'MCQ_Optional'),
      ('2101e9bf-539e-4833-8257-33eaaa599c38', 'Open'),
      ('58e61d3c-0cfb-4e26-be6d-a5c6eb44fe13', 'Boolean'),
      ('e8ca1aad-da3e-486d-b9b4-d1dbdc054dfe', 'Number'),
      ('febe2578-7f2f-4990-b382-ce583ef36681', 'Currency');`;


const QUESTIONS_TABLE_SEEDING = `INSERT INTO Questions(uuid, question, question_type_uuid, survey_uuid)
VALUES('2235692c-f794-4d9d-a2d3-65b6ca2c4a41', 'What app do you use most?', '51552849-e186-4160-90eb-f32ea6437d14', '14fef932-5cdd-426e-b877-80dfc45d6869'),
      ('f133e742-db30-4020-b82c-edae3ff33dbc', 'How much does it cost?', 'febe2578-7f2f-4990-b382-ce583ef36681', '14fef932-5cdd-426e-b877-80dfc45d6869'),
      ('88840fc3-1e64-46bb-b270-ed584c9c20f3', 'Whom will you vote? ', '9ca3c631-98bf-4c18-a5a7-d85f39a7869e',  '5844fa14-e632-47dc-beb7-7ce265144bc2'),
      ('848159f3-e8ca-4236-b3ca-5044e6f5713a', 'Are you hungry?', '58e61d3c-0cfb-4e26-be6d-a5c6eb44fe13', '60bf6a57-ff68-41ff-bb18-8cb1cbc5a181');`;


const BOOLEAN_ANSWERS_TABLE_SEEDING = `INSERT INTO Boolean_Answers(uuid, answer)
      VALUES('07eacaef-7b2a-4684-8bda-5369e2a99030', 'Yes'),
            ('ec6b0491-5ec2-4dd3-b5a7-6f006912cf5a', 'No'),
            ('656ef097-3ab6-432d-a435-9706b6f7f51b', 'Agree'),
            ('0059d92d-165a-4214-ba7d-9bd4d6c54d22', 'Disagree');`;


const ANSWERS_TABLE_SEEDING = `INSERT INTO Answers(uuid, answer, question_uuid)
VALUES('1ec8cb0e-1069-47ac-91f3-5d43f16a40f1', 'Entertainment', '2235692c-f794-4d9d-a2d3-65b6ca2c4a41'),
      ('7da3b370-67b1-4f4b-b1dd-fce77cab04c1', 30, 'f133e742-db30-4020-b82c-edae3ff33dbc'),
      ('24dc900d-04b3-44b0-b1e4-464c5da39a7c', 'NOTA', '88840fc3-1e64-46bb-b270-ed584c9c20f3'),
      ('1311ed5a-f2f2-4bdc-8efd-33c0ff2375e8', 'Yes', '848159f3-e8ca-4236-b3ca-5044e6f5713a');`;




const SURVEYS_AND_RESPONSE_TABLE_SEEDING = `INSERT INTO Surveys_And_Responses(uuid, survey_uuid, question_uuid, answer, user_uuid)
VALUES('30041c72-1d20-497f-adbb-7e5ecd85f462', '14fef932-5cdd-426e-b877-80dfc45d6869', '2235692c-f794-4d9d-a2d3-65b6ca2c4a41', 'random answer', '3006a28e-ad7c-4b35-974a-35f1fb6256ab'),
      ('8aac0e0e-17c3-48ea-a860-d00e25dc3fa0', '905a4f11-cc27-4fad-9337-0856788edda8', 'f133e742-db30-4020-b82c-edae3ff33dbc', 'Yes', 'b8784da4-e8f1-4329-bd4a-bddcefb2c8e3'),
      ('eeea1cfc-2d15-49c6-9896-8ec17b158ed1', '5844fa14-e632-47dc-beb7-7ce265144bc2', '88840fc3-1e64-46bb-b270-ed584c9c20f3', 'Quality is not good', '20c6e415-a2d8-4b1e-b90c-29ed01f14136'),
      ('29080e57-9d47-446e-acfa-7519c4d00ba9', '60bf6a57-ff68-41ff-bb18-8cb1cbc5a181', '848159f3-e8ca-4236-b3ca-5044e6f5713a', 'Open to all questions', '0ca04ce5-ade8-4d2a-8e8a-285be0578e18');`;




// eslint-disable-next-line no-unused-vars
const databaseSeeding = (async () => {
    try {
        const result = await executeQueryPromise(`SELECT COUNT(*) AS row FROM Surveys`);

        if (result[0].row === 0) {
            const promiseArray = [
                executeQueryPromise(SURVEYS_TABLE_SEEDING),
                executeQueryPromise(QUESTION_TYPES_TABLE_SEEDING),
                executeQueryPromise(QUESTIONS_TABLE_SEEDING),
                executeQueryPromise(ANSWERS_TABLE_SEEDING),
                executeQueryPromise(USERS_TABLE_SEEDING),
                executeQueryPromise(SURVEYS_AND_RESPONSE_TABLE_SEEDING),
                executeQueryPromise(SURVEY_CATEGORIES_TABLE_SEEDING),
                executeQueryPromise(BOOLEAN_ANSWERS_TABLE_SEEDING),
            ];

            await Promise.all(promiseArray)
        }
    } catch (error) {
        let errorMessage = 'Oops!! Error occurred:'
        handleDbError(errorMessage, error);
    }
});



const handleDbError = (errorMessage, error) => {
    const errorObj = {
        code: error.code,
        errno: error.errno,
        sqlMessage: error.sqlMessage,
        sqlState: error.sqlState,
        fatal: error.fatal,
    };
    console.error(errorMessage, "\n", errorObj);
}

module.exports = {
    databaseMigration,
};