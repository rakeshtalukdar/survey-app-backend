const express = require('express');
const { executeQueryPromise } = require('../dbConnection');
const uuid = require('uuid');
const { check, validationResult } = require('express-validator');

const router = express.Router();


router.get('/', async (req, res) => {
    const result = await executeQueryPromise(`SELECT Surveys.*, Survey_Categories.category AS category FROM Surveys INNER JOIN Survey_Categories 
    ON Surveys.survey_category_uuid = Survey_Categories.uuid;`);
    if (result.length === 0) {
        return res.status(404).json(({ errorMsg: "No Surveys Found" }));
    } else {
        res.json(result);
    }
})

router.get('/submitted-surveys', async (req, res) => {
    const result = await executeQueryPromise(`SELECT DISTINCT Surveys.*, Survey_Categories.category AS category   FROM Surveys  
    INNER JOIN Surveys_And_Responses
    ON Surveys.uuid = Surveys_And_Responses.survey_uuid 
     INNER JOIN Survey_Categories ON Surveys.survey_category_uuid = Survey_Categories.uuid WHERE Surveys.uuid = Surveys_And_Responses.survey_uuid;`);
    if (result.length === 0) {
        return res.status(404).json(({ errorMsg: "Survey Not Found" }));
    } else {
        res.json(result);
    }
})



router.get('/:id', async (req, res) => {
    const result = await executeQueryPromise(`SELECT * FROM Surveys WHERE uuid = '${req.params.id}'`);
    if (result.length === 0) {
        return res.status(404).json(({ errorMsg: "Survey Not Found" }));
    } else {
        res.json(result);
    }
})



router.post('/create', [
    check('title').isLength({ min: 8 }).trim().withMessage('Survey name must be minimum 8 characters long.'),
    check('survey_category_uuid').isLength({ min: 36, max: 36 }).withMessage('Please select a valid survey category.'),
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errorMsg: "Please check your input length or provide all the input fields." });
    } else {

        let openDate = new Date(req.body.openDate);
        let closeDate = new Date(req.body.closeDate);
        openDate = `${openDate.getFullYear()}-${openDate.getMonth() + 1}-${openDate.getDate()}`;
        closeDate = `${closeDate.getFullYear()}-${closeDate.getMonth() + 1}-${closeDate.getDate()}`;

        const newSurvey = {
            uuid: uuid.v4(),
            title: req.body.title,
            open_date: openDate,
            close_date: closeDate,
            survey_category_uuid: req.body.survey_category_uuid,
            status: 1,
        };

        try {
            const query = `INSERT INTO Surveys (uuid, title, survey_category_uuid, open_date, close_date, status)
                        VALUES('${newSurvey.uuid}', '${newSurvey.title}', '${newSurvey.survey_category_uuid}', '${newSurvey.open_date}', '${newSurvey.close_date}', '${newSurvey.status}');`;
            const result = await executeQueryPromise(query);
            if (result.affectedRows > 0) {
                res.send(newSurvey);
            } else {
                throw Error;
            }
        } catch (error) {
            res.status(500).json(({ errorMsg: "Sorry!! Unable to insert the record" }));
        }
    }
});


router.patch('/change-status', async (req, res) => {
    try {
        const { uuid, status } = req.body;

        if (status === 'created') {

            const result = await executeQueryPromise(`UPDATE Surveys SET status = 2 WHERE uuid = '${uuid}'`);
            if (result.affectedRows > 0) {
                return res.json({ msg: "Status Successfully updated" });
            } else {
                return res.status(400).json({ errorMsg: "No Content found" });
            }
        } else if (status === 'active') {

            const result = await executeQueryPromise(`UPDATE Surveys SET status = 3 WHERE uuid = '${uuid}'`);
            if (result.affectedRows > 0) {
                return res.json({ msg: "Status Successfully updated" });
            } else {
                return res.status(400).json({ errorMsg: "No Content found" });
            }
        } else {

            const result = await executeQueryPromise(`UPDATE Surveys SET status = 1 WHERE uuid = '${uuid}'`);
            if (result.affectedRows > 0) {
                return res.json({ msg: "Status Successfully updated" });
            } else {
                return res.status(400).json({ errorMsg: "No Content found" });
            }
        }

    } catch (error) {
        res.status(500).json(({ errorMsg: "Sorry!! Unable to insert the record" }));
    }
})




router.patch('/:id', [
    check('title').optional().isLength({ min: 8 }).trim().withMessage('Survey name must be minimum 8 characters long'),
    check('survey_category_uuid').optional().isLength({ min: 36, max: 36 }).withMessage('Please select a valid survey category')
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errorMsg: "Please check your input length or select correct fields." });
    } else {
        try {
            const getSurvey = await executeQueryPromise(`SELECT * FROM Surveys WHERE uuid = '${req.params.id}'`);
            if (getSurvey.length > 0) {
                const updateSurvey = req.body;
                const title = updateSurvey.title !== undefined ? updateSurvey.title : getSurvey[0].title;
                const survey_category_uuid = updateSurvey.survey_category_uuid !== undefined ? updateSurvey.survey_category_uuid : getSurvey[0].survey_category_uuid;

                await executeQueryPromise(`UPDATE Surveys SET title = '${title}', survey_category_uuid = '${survey_category_uuid}' WHERE uuid = '${req.params.id}'`);
                return res.json({ msg: 'Survey successfully updated.' });
            } else {
                return res.status(404).json(({ errorMsg: "Oops!! Unable to find the survey" }));
            }
        } catch (error) {
            res.status(500).json(({ errorMsg: "Oops!! Unable to update the survey" }));
        }
    }
})





router.delete('/:id', async (req, res) => {
    try {
        const result = await executeQueryPromise(`DELETE FROM Surveys WHERE uuid = '${req.params.id}'`);

        if (result.affectedRows > 0) {
            return res.json({ msg: 'Survey has been deleted' });
        }
        else {
            return res.status(404).json(({ errorMsg: "Oops!! Unable to find the survey" }));
        }
    } catch (error) {
        res.status(500).json(({ errorMsg: "Oops!! Unable to delete the record" }));
    }
})



/*###########################################################################
                    Routes for Questions
############################################################################*/



router.get('/:id/questions', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT * FROM Questions WHERE survey_uuid = '${req.params.id}'`);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})


router.get('/:id/questions/:questionId', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT * FROM Questions WHERE uuid = '${req.params.questionId}' AND survey_uuid = '${req.params.id}'`);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})





router.post('/:id/questions/create',
    [
        check('question').isLength({ min: 6 }).withMessage('Question length should be at least 6 characters.'),
        check('question_type').isIn(['Open', 'MCQ', 'MCQ_Optional', 'Boolean', 'Currency', 'Number']).withMessage('Please choose a valid question type.'),
        check('survey_uuid').isLength({ min: 36, max: 36 }).withMessage('Invalid survey.'),
    ],

    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.json({ errorMsg: "Please check your input length or provide all the input fields." });
        } else {
            try {
                const newQuestion = {
                    uuid: uuid.v4(),
                    question: req.body.question,
                    question_type: req.body.question_type,
                    survey_uuid: req.params.id,
                };
                const newAnswer = {
                    uuid: uuid.v4(),
                    mcqAns1: req.body.mcqAns1,
                    mcqAns2: req.body.mcqAns2,
                    mcqAns3: req.body.mcqAns3,
                    mcqAns4: req.body.mcqAns4,
                    booleanAns1: req.body.booleanAns1,
                    booleanAns2: req.body.booleanAns2,
                    mcqOptionalAns1: req.body.mcqOptionalAns1,
                    mcqOptionalAns2: req.body.mcqOptionalAns2,
                    mcqOptionalAns3: req.body.mcqOptionalAns3,
                    mcqOptionalAns4: req.body.mcqOptionalAns4,
                    numberAns: req.body.numberAns,
                    currencyAns: req.body.currencyAns,
                }

                const getQuestionTypeUuid = await executeQueryPromise(`SELECT uuid FROM Question_Types WHERE type = '${newQuestion.question_type}'`);

                switch (newQuestion.question_type) {
                    case 'Open':
                        await executeQueryPromise(`INSERT INTO Questions (uuid, question, question_type_uuid, survey_uuid)
                    VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}');`);

                        await executeQueryPromise(`INSERT INTO Answers (uuid, answer, question_uuid)
                    VALUES('${uuid.v4()}', '', '${newQuestion.uuid}');`);
                        break;

                    case 'Number':
                        await executeQueryPromise(`INSERT INTO Questions (uuid, question, question_type_uuid, survey_uuid)
                        VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}');`);

                        await executeQueryPromise(`INSERT INTO Answers (uuid, answer, question_uuid)
                        VALUES('${uuid.v4()}', '', '${newQuestion.uuid}');`);

                        break;

                    case 'Currency':
                        await executeQueryPromise(`INSERT INTO Questions(uuid, question, question_type_uuid, survey_uuid)
                        VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}'); `);

                        await executeQueryPromise(`INSERT INTO Answers(uuid, answer, question_uuid)
                        VALUES('${newAnswer.uuid}', '${newAnswer.currencyAns}', '${newQuestion.uuid}'); `);
                        break;

                    case 'MCQ':

                        executeQueryPromise(`INSERT INTO Questions(uuid, question, question_type_uuid, survey_uuid)
                        VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}'); `)
                            .then(() => {
                                executeQueryPromise(`INSERT INTO Answers(uuid, answer, question_uuid)
                        VALUES('${uuid.v4()}', '${newAnswer.mcqAns1}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqAns2}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqAns3}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqAns4}', '${newQuestion.uuid}');`)
                                    .then(() => {
                                        return res.json({ msg: 'Data Successfully inserted' });
                                    })
                                    .catch((error) => { throw error })
                            })
                            .catch(() => {
                                res.status(500).json(({
                                    errorMsg: "Sorry!! Unable to insert the record",
                                }));
                            });
                        break;

                    case 'MCQ_Optional':
                        executeQueryPromise(`INSERT INTO Questions(uuid, question, question_type_uuid, survey_uuid)
                        VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}'); `)
                            .then(() => {
                                executeQueryPromise(`INSERT INTO Answers(uuid, answer, question_uuid)
                        VALUES('${uuid.v4()}', '${newAnswer.mcqOptionalAns1}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqOptionalAns2}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqOptionalAns3}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.mcqOptionalAns4}', '${newQuestion.uuid}'); `)
                                    .then(() => {
                                        return res.json({ msg: 'Data Successfully inserted' });
                                    })
                                    .catch((error) => { throw error });
                            })
                            .catch(() => res.status(500).json({
                                errorMsg: "Sorry!! Unable to insert the record",
                            }));
                        break;

                    case 'Boolean':
                        executeQueryPromise(`INSERT INTO Questions(uuid, question, question_type_uuid, survey_uuid)
                        VALUES('${newQuestion.uuid}', '${newQuestion.question}', '${getQuestionTypeUuid[0].uuid}', '${newQuestion.survey_uuid}'); `)
                            .then(() => {
                                executeQueryPromise(`INSERT INTO Answers(uuid, answer, question_uuid)
                        VALUES('${uuid.v4()}', '${newAnswer.booleanAns1}', '${newQuestion.uuid}'),
                            ('${uuid.v4()}', '${newAnswer.booleanAns2}', '${newQuestion.uuid}'); `)
                                    .then(() => {
                                        return res.json({ msg: 'Data Successfully inserted' });
                                    })
                                    .catch((error) => { throw error });
                            })
                            .catch(() => res.status(500).json({
                                errorMsg: "Sorry!! Unable to insert the record",
                            }));
                        break;

                    default: break;
                }
                res.json({ msg: 'Data Successfully inserted' });
            } catch (error) {
                res.status(500).json(({
                    errorMsg: "Sorry!! Unable to insert the record",
                    error,
                }));
            }
        }
    })





router.patch('/:id/questions/:questionId', [
    check('question').optional().isLength({ min: 6 }).withMessage('Question length should be at least 6 characters.'),
    check('question_type_uuid').optional().isLength({ min: 36, max: 36 }).withMessage('Please choose a valid question type.'),
    check('survey_uuid').optional().isLength({ min: 36, max: 36 }).withMessage('Invalid survey.'),
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errorMsg: "Please check your input length or select correct fields." });
    } else {
        try {
            const getQuestion = await executeQueryPromise(`SELECT * FROM Questions WHERE uuid = '${req.params.questionId}' AND survey_uuid = '${req.params.id}'`);

            if (getQuestion.length > 0) {
                const updateQuestion = req.body;
                const question = updateQuestion.question !== undefined ? updateQuestion.question : getQuestion[0].question;
                const question_type_uuid = updateQuestion.question_type_uuid !== undefined ? updateQuestion.question_type_uuid : getQuestion[0].question_type_uuid;
                const survey_uuid = updateQuestion.survey_uuid !== undefined ? updateQuestion.survey_uuid : getQuestion[0].survey_uuid;

                await executeQueryPromise(`UPDATE Questions SET question = '${question}', question_type_uuid = '${question_type_uuid}', survey_uuid = '${survey_uuid}' WHERE uuid = '${req.params.questionId}'`);
                return res.json({ msg: 'Question successfully updated.' });

            } else {
                return res.status(404).json(({ errorMsg: "Oops!! Unable to find the question" }));
            }
        } catch (error) {
            res.status(500).json(({ errorMsg: "Oops!! Unable to update the question" }));
        }
    }
})



router.delete('/:id/questions/:questionId', async (req, res) => {

    try {
        const query = `DELETE FROM Questions WHERE uuid = '${req.params.questionId}' AND survey_uuid = '${req.params.id}'`;
        const result = await executeQueryPromise(query);
        if (result.affectedRows > 0) {
            return res.json({ msg: 'Question has been deleted' });
        } else {
            return res.json(({ msg: "Oops!! Unable to find the question" }));
        }
    } catch (error) {
        res.json(({ errorMsg: "Oops!! Unable to delete the question" }));
    }
})


/*###########################################################################
                    Routes for Answers
############################################################################*/

router.get('/:id/answers', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT Answers.* FROM Answers INNER JOIN Questions
                        ON Answers.question_uuid = Questions.uuid INNER JOIN Surveys
                        ON Questions.survey_uuid = Surveys.uuid
                        WHERE Surveys.uuid = '${req.params.id}'; `);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})


router.get('/:id/answers/:answerId', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT Answers.* FROM Answers INNER JOIN Questions
                        ON Answers.question_uuid = Questions.uuid INNER JOIN Surveys
                        ON Questions.survey_uuid = Surveys.uuid
                        WHERE Surveys.uuid = '${req.params.id}' AND Answers.uuid = '${req.params.answerId}'; `);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})



router.post('/:id/answers/create', [
    check('answer').isLength({ min: 2 }).trim().withMessage('Your answer must be minimum 2 characters long.'),
    check('question_uuid').isLength({ min: 36, max: 36 }).withMessage('Your question did not match.'),
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errorMsg: "Please check your input length or provide all the input fields." });
    } else {
        const newAnswer = {
            uuid: uuid.v4(),
            answer: req.body.answer,
            question_uuid: req.body.question_uuid,
        };

        try {
            const query = `INSERT INTO Answers (uuid, answer, question_uuid)
                        VALUES('${newAnswer.uuid}', '${newAnswer.answer}', '${newAnswer.question_uuid}');`;
            const result = await executeQueryPromise(query);
            if (result.affectedRows > 0) {
                res.send(newAnswer);
            } else {
                throw Error;
            }
        } catch (error) {
            res.status(500).json(({ errorMsg: "Sorry!! Unable to insert the record" }));
        }
    }
});

router.patch('/:id/answers/:answerId/edit', async (req, res) => {
    const answerId = req.params.answerId;
    const updateAnswer = {
        answer: req.body.answer,
        question_uuid: req.body.question_uuid,
    };

    const query = `UPDATE Answers SET answer = '${updateAnswer.answer}' WHERE uuid = '${answerId}';`;
    const result = await executeQueryPromise(query);
    if (result.affectedRows > 0) {
        res.send(updateAnswer);
    } else {
        throw Error;
    }

});




router.delete('/:id/answers/:answerId', async (req, res) => {
    try {
        const result = await executeQueryPromise(`DELETE FROM Answers WHERE uuid = '${req.params.answerId}' AND question_uuid = '${req.params.id}'`);

        if (result.affectedRows > 0) {
            return res.json({ msg: 'Answer has been deleted' });
        } else {
            return res.status(404).json(({ errorMsg: "Oops!! Unable to find the answer" }));
        }

    } catch (error) {
        res.status(500).json(({ errorMsg: "Oops!! Unable to delete the answer" }));
    }
})



module.exports = router;
