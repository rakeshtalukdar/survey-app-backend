const config = require('./config');
const mysql = require('mysql');

const connection = mysql.createPool({
    host: config.MYSQL.HOST,
    user: config.MYSQL.USER,
    password: config.MYSQL.PASSWORD,
    database: config.MYSQL.DATABASE,
});


// eslint-disable-next-line no-unused-vars
const executeQueryPromise = (query) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (error, results) => {
            if (error) {
                reject(error);
            } else {
                resolve(results);
            }
        })
    });
};

module.exports = {
    connection,
    executeQueryPromise,
}