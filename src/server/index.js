const express = require('express');
var cors = require('cors')
const config = require('./config');
const { executeQueryPromise } = require('./dbConnection');
const { databaseMigration } = require('./dbMigrationAndSeeding');
const surveyRoutes = require('./Routes/surveys');
const uuid = require('uuid');
// Migrating and seeding the database
databaseMigration();


const PORT = config.PORT;
const app = express();
app.use(cors())


//Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));




app.get('/', async (req, res) => {
    const result = await executeQueryPromise(`SELECT Surveys.*, Survey_Categories.category AS category FROM Surveys INNER JOIN Survey_Categories 
    ON Surveys.survey_category_uuid = Survey_Categories.uuid;`);
    if (result.length === 0) {
        return res.status(404).json(({ errorMsg: "No Surveys Found" }));
    } else {
        res.json(result);
    }
})

app.use('/surveys', surveyRoutes);


app.get('/survey-categories', async (req, res) => {
    const result = await executeQueryPromise(`SELECT * FROM Survey_Categories`);
    if (result.length === 0) {
        return res.status(404).json(({ errorMsg: "Survey Categories Not Found" }));
    } else {
        res.json(result);
    }
})


app.get('/active/surveys', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT Surveys.*, Survey_Categories.category AS category FROM Surveys INNER JOIN Survey_Categories 
        ON Surveys.survey_category_uuid = Survey_Categories.uuid  WHERE status = 2;`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no active survey." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})



app.get('/closed/surveys', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT Surveys.*, Survey_Categories.category AS category FROM Surveys INNER JOIN Survey_Categories 
        ON Surveys.survey_category_uuid = Survey_Categories.uuid  WHERE status = 3;`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no closed survey." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})



app.get('/question-types', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT * FROM Question_Types`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no active survey." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})


app.get('/:id/all-questions', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT Questions.uuid AS uuid, Questions.question, Question_Types.type AS qType FROM Questions 
        INNER JOIN Question_Types ON Questions.question_type_uuid = Question_Types.uuid WHERE survey_uuid = '${req.params.id}';`);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})

app.get('/all-answers', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT * FROM Answers`);
        res.json(result);
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})



app.post('/:id/save-response', async (req, res) => {
    try {
        let data = { ...req.body }
        let dataLength = Object.keys(data.response).length - 1;
        let counter = 0;
        const userEmail = data.user;
        const userUuid = uuid.v4();

        await executeQueryPromise(`INSERT INTO Users (uuid, email) VALUES('${userUuid}', '${userEmail}')`);
        let response = {};
        if (data !== undefined) {
            for (let value in data.response) {
                if (value !== 'user') {

                    response.uuid = uuid.v4();
                    response.survey_uuid = req.params.id;
                    response.question_uuid = value;
                    response.answer = data.response[value];
                    const result = await executeQueryPromise(`INSERT INTO Surveys_And_Responses(uuid, survey_uuid,  question_uuid, answer, user_uuid)
                        VALUES('${response.uuid}', '${response.survey_uuid}', '${response.question_uuid}', '${response.answer}', '${userUuid}');`);
                    counter += result.affectedRows;
                }
            }
        } else {
            res.send('No data to insert')
        }
        if (counter === dataLength) {
            res.status(200).json({ msg: 'Data Successfully inserted' })
        } else {
            throw Error;
        }
    } catch (error) {
        res.status(500).json(({ errorMsg: "Sorry!! Unable to insert the record" }));
    }

})


app.get('/users', async (req, res) => {
    try {
        const result = await executeQueryPromise(`SELECT DISTINCT email, uuid FROM Users`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no user." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})


app.get('/user-submitted-surveys/:id', async (req, res) => {
    const userId = req.params.id;
    try {
        const result = await executeQueryPromise(`SELECT Surveys.* FROM Surveys  
        INNER JOIN Surveys_And_Responses
        ON Surveys.uuid = Surveys_And_Responses.survey_uuid 
        INNER JOIN Users WHERE '${userId}' = Surveys_And_Responses.user_uuid AND Surveys.uuid = Surveys_And_Responses.survey_uuid GROUP BY Surveys.uuid;`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no user." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})

app.get('/user-review-survey/:id/user/:userId', async (req, res) => {
    const userId = req.params.userId;
    const surveyId = req.params.id;
    try {
        const result = await executeQueryPromise(`SELECT Surveys_And_Responses.uuid, Surveys_And_Responses.answer AS answer, 
        Questions.question AS question FROM Surveys_And_Responses INNER JOIN Questions  
        ON Questions.uuid = Surveys_And_Responses.question_uuid 
        WHERE '${userId}' = Surveys_And_Responses.user_uuid AND '${surveyId}' = Surveys_And_Responses.survey_uuid;`);
        if (result.length > 0) {
            return res.json(result);
        } else {
            return res.json({ msg: "There is no user." })
        }
    } catch (error) {
        return res.status(404).json(({ errorMsg: "Request Not Found" }));
    }
})



app.listen(PORT, () => console.log(`Server started at ${PORT}`));
