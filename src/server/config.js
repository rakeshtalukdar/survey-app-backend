const path = require('path');

require('dotenv').config({
    path: path.join('..', '..', '.env'),
});

module.exports = {
    PORT: process.env.PORT || 8000,
    MYSQL: {
        HOST: process.env.HOST || 'localhost',
        USER: process.env.DATABASE_USER,
        PASSWORD: process.env.PASSWORD,
        DATABASE: process.env.DATABASE,
    },
};

